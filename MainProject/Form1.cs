using System.Data;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace App_01
{
    public partial class Form1 : Form
    {
        
        #region delegate
        private delegate void ChangeDataSourceGridView(DataTable dt);
        #endregion
        public Form1()
        {
            InitializeComponent();
            

        }
        public DataGridView dgv { get { return dataGridView1; } }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private async void Btn_Cerca_Click(object sender, EventArgs e)
        {
            Utility utility = new Utility();
            utility._form1 = this;
            Task task = Task.Run(() => utility.Load_DB());
            await task;
            textBoxTotaleRisultati.Text = utility.Totale_risultati_GridView(dataGridView1);
        }
        private void Load_DB()
        {
            try
            {
                if (dataGridView1.Rows.Count > 0)
                {
                    MessageBox.Show("hai gia cercato!!");

                }
                else
                {
                    string connessione = System.Configuration.ConfigurationManager.ConnectionStrings["AdventureWork2019"].ConnectionString;
                    string query = "SELECT* FROM Person.Person";
                    DataSet ds = new DataSet();
                    //using chiude automaticamente la connessione in quanto impplementa il pattern Dispose();
                    using (SqlConnection conn = new SqlConnection(connessione))
                    {
                        conn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(query, connessione);
                        adapter.Fill(ds);
                        
                   
                    }
                    DataTable dt = ds.Tables[0];
                    SetDataSourceDataGridView(dt);
                    
                   
                }
              
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
       private string Totale_risultati_GridView(DataGridView dv)
        {
            int totale = Convert.ToInt32(dv.Rows.Count);
            return Convert.ToString(totale);
        }
       #region methodDelegate
        public void SetDataSourceDataGridView(DataTable table)
        {
            if(InvokeRequired)
            {
                dataGridView1.Invoke(new ChangeDataSourceGridView(SetDataSourceDataGridView), new object[] {table});
            }
            else
            {
                dataGridView1.DataSource = table;
            }
        }
        #endregion

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnInserisci_Click(object sender, EventArgs e)
        {
            Form2 _form2 = new Form2(BtnInserisci.Text);
            _form2.ClickAdd += _form2_ClickAdd;
            _form2.ShowDialog();
        }

        private async void _form2_ClickAdd(object? sender, EventArgs e)
        {
            Utility utility = new Utility();
            utility._form1 = this;
            Task task = Task.Run(() => utility.Load_DB());
            await task;
            textBoxTotaleRisultati.Text = utility.Totale_risultati_GridView(dataGridView1);

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBoxRisultato_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void BtnCancella_Click(object sender, EventArgs e)
        {
            Form2 _form2 = new Form2(BtnCancella.Text);
            _form2.ClickCancel += _form2_ClickCancel;
            _form2.ShowDialog();
            
        }

        private async void _form2_ClickCancel(object? sender, EventArgs e)
        {
            Utility utility = new Utility();
            utility._form1 = this;
            Task task = Task.Run(() => utility.Load_DB());
            await task;
            textBoxTotaleRisultati.Text = utility.Totale_risultati_GridView(dataGridView1);

        }

        private void BtnAggiorna_Click(object sender, EventArgs e)
        {
            Form2 _form2 = new Form2(BtnAggiorna.Text);
            _form2.ClickUpdate += _form2_ClickUpdate;
            _form2.ShowDialog(this);
        }

        private async void _form2_ClickUpdate(object? sender, EventArgs e)
        {
            Utility utility = new Utility();
            utility._form1 = this;
            Task task = Task.Run(() => utility.Load_DB());
            await task;
            textBoxTotaleRisultati.Text = utility.Totale_risultati_GridView(dataGridView1);
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TotaleRisultati_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}