﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_01
{
    internal class Utility
    {
        public Form1 _form1;
        public Form2 _form2;
        #region delegate
        private delegate void ChangeDataSourceGridView(DataTable dt);
        #endregion
        #region Load_DB
        public void Load_DB()
        {
            try
            {
             
                    try
                    {
                        string connessione = System.Configuration.ConfigurationManager.ConnectionStrings["Prova"].ConnectionString;
                        string query = "SELECT* FROM [Prova].[dbo].[Studente]";
                        DataSet ds = new DataSet();
                        //using chiude automaticamente la connessione in quanto implementa il pattern Dispose();
                        using (SqlConnection conn = new SqlConnection(connessione))
                        {
                            conn.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(query, connessione);
                            adapter.Fill(ds);
                        }
                        DataTable dt = ds.Tables[0];
                        SetDataSourceDataGridView(dt);


                    }
                    catch (SqlException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion LoadDB
        public string Totale_risultati_GridView(DataGridView dv)
        {
            int totale = Convert.ToInt32(dv.Rows.Count);
            return Convert.ToString(totale);
        }
        #region methodDelegate
        public  void SetDataSourceDataGridView(DataTable table)
        {
            if (_form1.InvokeRequired)
            {
                _form1.dgv.Invoke(new ChangeDataSourceGridView(SetDataSourceDataGridView), new object[] { table });
            }
            else
            {
                _form1.dgv.DataSource = table;
            }
        }
        #endregion
      public void ShowCancelGuiDB()
        {
            #region DefineGUI
            _form2.SetTxtEnabled_1(true);
            _form2.SetTxtEnabled_2(false);
            _form2.SetTxtEnabled_3(false);
            _form2.SetTxtEnabled_4(false);
            _form2.SetTxtEnabled_5(false);
            _form2.SetTxtEnabled_6(false);
            _form2.SetDateTimePickerEnabled_7(false);
            _form2.SetTxtButton("Cancella");
            #endregion
       

        }
        public void ShowInsertGuiDB()
        {
            #region DefineGUI
            _form2.SetTxtEnabled_1(true);
            _form2.SetTxtEnabled_2(true);
            _form2.SetTxtEnabled_3(true);
            _form2.SetTxtEnabled_4(true);
            _form2.SetTxtEnabled_5(true);
            _form2.SetTxtEnabled_6(true);
            _form2.SetDateTimePickerEnabled_7(true);
        
            _form2.SetTxtButton("Inserisci");
            #endregion
        }
        public void ShowUpdateGuiDB()
        {
            #region DefineGUI
            _form2.SetTxtEnabled_1(true);
            _form2.SetTxtEnabled_2(true);
            _form2.SetTxtEnabled_3(true);
            _form2.SetTxtEnabled_4(true);
            _form2.SetTxtEnabled_5(true);
            _form2.SetTxtEnabled_6(true);
            _form2.SetDateTimePickerEnabled_7(false);
            _form2.SetTxtButton("Aggiorna");
            _form2.SetLabel_1("Inserisci la Matricola che vuoi Aggiornare");
            #endregion
        }
        public int InserisciRecordDB()
        {
            try
            {
                int numero_record_inseriti = 0;
                string Matricola =_form2.GetTxt_1.Text;
                string Nome = _form2.GetTxt_2.Text; 
                string Cognome = _form2.GetTxt_3.Text; 
                int Età = Convert.ToInt32(_form2.GetTxt_4.Text); 
                string Indirizzo = _form2.GetTxt_5.Text;
                string telefono= _form2.GetTxt_6.Text;
                DateTime PeriodoIscrizione = Convert.ToDateTime(_form2.DateTimePicker.Value);
                string connessione = System.Configuration.ConfigurationManager.ConnectionStrings["Prova"].ConnectionString;
                string query = "INSERT INTO [Prova].[dbo].[Studente]([Matricola],[Nome],[Cognome],[Età],[Indirizzo],[telefono],[PeriodoIscrizione]) VALUES (@Matricola,@Nome,@Cognome,@Età,@Indirizzo,@telefono,@PeriodoIscrizione)";
                try
                {
                    using (SqlConnection conn = new SqlConnection(connessione))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@Matricola", Matricola);
                        cmd.Parameters.AddWithValue("@Nome", Nome);
                        cmd.Parameters.AddWithValue("@Cognome", Cognome);
                        cmd.Parameters.AddWithValue("@Età",Età);
                        cmd.Parameters.AddWithValue("@Indirizzo", Indirizzo);
                        cmd.Parameters.AddWithValue("@telefono", telefono);
                        cmd.Parameters.AddWithValue("@PeriodoIscrizione", PeriodoIscrizione);
                        numero_record_inseriti = cmd.ExecuteNonQuery();
                    }
                }
                catch(SqlException sql)
                {
                    Console.WriteLine(sql.Message); 
                }
                if (numero_record_inseriti > 0)
                {
                    MessageBox.Show("Inserimento effettuato correttamente!");

                }
                else
                {
                    MessageBox.Show("Inserimento fallito...");
                }
                return numero_record_inseriti;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message); 
                return 0;
            }
           

        }
        public  void DeleteRecordDB()
        {
            try
            {
                int numero_record_eliminati;
                string matricola = _form2.GetTxt_1.Text;
                string connessione = System.Configuration.ConfigurationManager.ConnectionStrings["Prova"].ConnectionString;
                string query = "DELETE FROM [Prova].[dbo].[Studente] WHERE Matricola=@Matricola";
                using (SqlConnection conn = new SqlConnection(connessione))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Matricola", matricola);
                    numero_record_eliminati = cmd.ExecuteNonQuery();
                }
                if (numero_record_eliminati > 0)
                {
                    MessageBox.Show("Eliminazione effettuata correttamente");

                }
                
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void AggiornaRecordDB()
        {
            try
            {
                int numero_record_aggiornati = 0;
                string Matricola = _form2.GetTxt_1.Text;
                string Nome = _form2.GetTxt_2.Text;
                string Cognome = _form2.GetTxt_3.Text;
                int Età = Convert.ToInt32(_form2.GetTxt_4.Text);
                string Indirizzo = _form2.GetTxt_5.Text;
                string telefono = _form2.GetTxt_6.Text;
               // DateTime PeriodoIscrizione = Convert.ToDateTime(_form2.DateTimePicker.Value);
                string connessione = System.Configuration.ConfigurationManager.ConnectionStrings["Prova"].ConnectionString;
                string query = "UPDATE [Prova].[dbo].[Studente] SET Nome=@Nome,Cognome=@Cognome,Età=@Età,Indirizzo=@Indirizzo,telefono=@telefono WHERE Matricola=@Matricola";
                try
                {
                    using (SqlConnection conn = new SqlConnection(connessione))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@Matricola", Matricola);
                        cmd.Parameters.AddWithValue("@Nome", Nome);
                        cmd.Parameters.AddWithValue("@Cognome", Cognome);
                        cmd.Parameters.AddWithValue("@Età", Età);
                        cmd.Parameters.AddWithValue("@Indirizzo", Indirizzo);
                        cmd.Parameters.AddWithValue("@telefono", telefono);
                        //cmd.Parameters.AddWithValue("@PeriodoIscrizione", PeriodoIscrizione);
                        numero_record_aggiornati = cmd.ExecuteNonQuery();
                    }
                }
                catch (SqlException sql)
                {
                    Console.WriteLine(sql.Message);
                }
                if (numero_record_aggiornati > 0)
                {
                    MessageBox.Show("Aggiornamento effettuato correttamente!");

                }
                else
                {
                    MessageBox.Show("Studente non presente!Aggiornamento fallito.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

    }

}
