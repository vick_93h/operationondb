﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace App_01
{
    public partial class Form2 : Form
    {
        #region RoundedCorner
        // [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        // private static extern IntPtr CreateRoundRectRgn
        //(
        //    int nLeftRect,     // x-coordinate of upper-left corner
        //    int nTopRect,      // y-coordinate of upper-left corner
        //    int nRightRect,    // x-coordinate of lower-right corner
        //    int nBottomRect,   // y-coordinate of lower-right corner
        //    int nWidthEllipse, // height of ellipse
        //    int nHeightEllipse // width of ellipse
        //);
        #endregion

        public static string Tipo;
        public event EventHandler ClickCancel;
        public event EventHandler ClickAdd;
        public event EventHandler ClickUpdate;
        protected virtual void OnClickCancel(EventArgs e) => ClickCancel?.Invoke(this, e);
        protected virtual void OnClickAdd(EventArgs e) => ClickAdd?.Invoke(this, e);
        protected virtual void OnClickUpdate(EventArgs e) => ClickUpdate?.Invoke(this, e);

        #region Getter&Setter
        public TextBox GetTxt_1 { get { return textBox1; } }
        public TextBox GetTxt_2 { get { return textBox2; } }
        public TextBox GetTxt_3 { get { return textBox3; } }
        public TextBox GetTxt_4 { get { return textBox4; } }
        public TextBox GetTxt_5 { get { return textBox5; } }
        public TextBox GetTxt_6 { get { return textBox6; } }
        public DateTimePicker DateTimePicker { get { return dateTimePicker1; } }
        public void SetLabel_1(string value) { label1.Text = value;} 
        public void SetTxtEnabled_1(bool value) { textBox1.Enabled = value; }
        public void SetTxt_1(string value) { textBox1.Text = value; }
        public void SetTxtEnabled_2(bool value) { textBox2.Enabled = value; }
        public void SetTxtEnabled_3(bool value) { textBox3.Enabled = value; }
        public void SetTxtEnabled_4(bool value) { textBox4.Enabled = value; }
        public void SetTxtEnabled_5(bool value) { textBox5.Enabled = value; }
        public void SetTxtEnabled_6(bool value) { textBox6.Enabled = value; }
        public void SetDateTimePickerEnabled_7(bool value) { dateTimePicker1.Enabled = value; }
        public void SetTxtButton(String Text) { BtnOP.Text = Text; }
        #endregion
       
        public Form2(string TipoBtn)
        {
            InitializeComponent();
            Tipo = TipoBtn;
            //istruzioni per lanciare Rounded Corner
            //this.FormBorderStyle = FormBorderStyle.None;
            //Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
        }

        
        private void Form2_Load(object sender,EventArgs e)
        {
            switch (Tipo)
            {
             case "Cancella":   Utility utilita = new Utility();
                                utilita._form2 = this;
                                utilita.ShowCancelGuiDB();
                                break;
             case "Inserisci":  Utility utilita_1 = new Utility();
                                utilita_1._form2 = this;
                                utilita_1.ShowInsertGuiDB();
                                break;
            case "Aggiorna":   Utility utilita_2 = new Utility();
                               utilita_2._form2 = this;
                               utilita_2.ShowUpdateGuiDB();
                               break;


            }
                
            

        }

        //private void Form2_Load(object sender, TipoFormEventArgs e)
        //{
            
            

        //}

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
      

        private void BtnOP_Click(object sender, EventArgs e)
        {
            switch (Tipo)
            {
            case "Cancella":Utility utilita = new Utility();
                            utilita._form2 = this;
                            utilita.DeleteRecordDB();
                            this.OnClickCancel(e);
                            break;
            case "Inserisci": Utility utilita_1 = new Utility();
                              utilita_1._form2 = this;
                             int num_record_inseriti= utilita_1.InserisciRecordDB();
                             if (num_record_inseriti > 0)
                               {
                               this.OnClickAdd(e);
                               }
                            break;
            case "Aggiorna":Utility utilita_2 = new Utility();
                            utilita_2._form2 = this;
                            utilita_2.AggiornaRecordDB();
                            this.OnClickUpdate(e);
                            break;
            }
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
         
        }

        private void Form2_KeyUp(object sender, KeyEventArgs e)
        {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl(ActiveControl, true, true, true, true);
                }
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
